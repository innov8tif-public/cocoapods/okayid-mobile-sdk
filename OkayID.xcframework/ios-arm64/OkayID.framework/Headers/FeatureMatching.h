//
//  FeatureMatching.h
//  OCRTest
//
//  Created by Ban Er Win on 09/02/2023.
//

#ifndef FeatureMatching_h
#define FeatureMatching_h

#include <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

enum idType {
    MYS_MYKAD,
    MYS_MYTENTERA
};

extern enum idType idTypeTemplate;

@interface FeatureMatching : NSObject

- (UIImage *) featureMatching:(UIImage*) image idType:(NSString*) idType;
- (NSString *)idTypeToString:(enum idType)ID;


@end

NS_ASSUME_NONNULL_END

#endif /* FeatureMatching_h */

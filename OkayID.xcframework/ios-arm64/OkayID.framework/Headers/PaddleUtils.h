//
//  PaddleUtils.h
//
//  Created by Ban Er Win on 27/07/2022.
//
#ifndef PaddleUtils_h
#define PaddleUtils_h

#include <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//OpenCV

NS_ASSUME_NONNULL_BEGIN

@interface PaddleUtils: NSObject
+ (NSString *)openCVVersionString;

//TorchModule Image Segmentation
- (nullable instancetype)initWithFileAtPath:(NSString*)filePath
    NS_SWIFT_NAME(init(fileAtPath:))NS_DESIGNATED_INITIALIZER;
+ (instancetype)new NS_UNAVAILABLE;
- (NSMutableArray *)segmentImage:(void*)imageBuffer withWidth:(int)width withHeight:(int)height
   NS_SWIFT_NAME(segment(image:withWidth:withHeight:));

@end

NS_ASSUME_NONNULL_END

#endif /* PaddleUtils_h */


// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.7.1 (swiftlang-5.7.1.135.3 clang-1400.0.29.51)
// swift-module-flags: -target arm64-apple-ios15.5 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name OkayID
// swift-module-flags-ignorable: -enable-bare-slash-regex
import AVFoundation
import CoreGraphics
import CoreImage
import CoreLocation
import CoreML
import CoreMotion
import CryptoSwift
import Foundation
import ImageIO
import MobileCoreServices
@_exported import OkayID
import Photos
import PhotosUI
import Swift
import UIKit
import Vision
import WebKit
import _Concurrency
import _StringProcessing
extension Swift.String {
  public subscript(value: Swift.Int) -> Swift.Character {
    get
  }
}
extension Swift.String {
  public subscript(value: Foundation.NSRange) -> Swift.Substring {
    get
  }
}
extension Swift.String {
  public subscript(value: Swift.CountableClosedRange<Swift.Int>) -> Swift.Substring {
    get
  }
  public subscript(value: Swift.CountableRange<Swift.Int>) -> Swift.Substring {
    get
  }
  public subscript(value: Swift.PartialRangeUpTo<Swift.Int>) -> Swift.Substring {
    get
  }
  public subscript(value: Swift.PartialRangeThrough<Swift.Int>) -> Swift.Substring {
    get
  }
  public subscript(value: Swift.PartialRangeFrom<Swift.Int>) -> Swift.Substring {
    get
  }
  public func matches(_ regex: Swift.String) -> Swift.Bool
  public func index(at offset: Swift.Int) -> Swift.String.Index
}
@_hasMissingDesignatedInitializers public class Passport {
  @objc deinit
}
@_hasMissingDesignatedInitializers public class MyTentera {
  @objc deinit
}
@_hasMissingDesignatedInitializers public class MyKad {
  @objc deinit
}
@_hasMissingDesignatedInitializers public class OkayIDSDK {
  @objc deinit
}
@_hasMissingDesignatedInitializers public class OkayIDDoc {
  public typealias FilesCompletion = (Foundation.URL?, Swift.String?, OkayID.OkayIDError?) -> Swift.Void
  public static func start(okayIDConfig config: OkayID.OkayIDConfig, license: Swift.String, _ completion: @escaping OkayID.OkayIDDoc.FilesCompletion)
  @objc deinit
}
public class OkayIDConfig {
  public var showOverlay: Swift.Bool
  public var id: Swift.String
  public var captureConfigPair: OkayID.CaptureConfigPair
  public var frame: OkayID.OkayIDFrameConfig
  public var fullScreen: Swift.Bool
  public var captureBtnColor: UIKit.UIColor
  public var imageQuality: CoreFoundation.CGFloat
  public init(viewController: UIKit.UIViewController)
  @objc deinit
}
public struct OkayIDFrameConfig {
  public init(size: CoreFoundation.CGSize? = nil, color: UIKit.UIColor = UIColor.white, content: Foundation.URL? = nil)
  public var size: CoreFoundation.CGSize?
  public var color: UIKit.UIColor
  public var content: Foundation.URL?
}
public struct CaptureConfigPair {
  public init(frontPhoto: OkayID.OkayIDCaptureConfig = OkayIDCaptureConfig(), secondPhoto: OkayID.OkayIDCaptureConfig? = nil)
  public var frontPhoto: OkayID.OkayIDCaptureConfig
  public var secondPhoto: OkayID.OkayIDCaptureConfig?
}
public struct OkayIDCaptureConfig {
  public init(outputPath: Foundation.URL? = nil)
  public var outputPath: Foundation.URL?
}
public func convertImageToBase64(fileUrl: Foundation.URL) throws -> Swift.String
public enum OkayIDError : Swift.Error {
  case invalidConfig(_: Swift.String)
  case imageCapture(_: Swift.String)
  case userCancelled
  case invalidLicense
  case noError
  case cameraPermission
  case camera(_: Swift.String)
  case miscellaneous(_: Swift.String)
  case unrecognisedIDType(_: Swift.String)
}
public struct IDType {
  public static let MYS_MYKAD: Swift.String
  public static let PASSPORT_MRZ: Swift.String
  public static let MYS_MYPR: Swift.String
  public static let MYS_MYKAS: Swift.String
  public static let MYS_MYTENTERA: Swift.String
}
public enum CameraState {
  case ready, accessDenied, noDeviceFound, notDetermined
  public static func == (a: OkayID.CameraState, b: OkayID.CameraState) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum CameraDevice {
  case front, back
  public static func == (a: OkayID.CameraDevice, b: OkayID.CameraDevice) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum CameraFlashMode : Swift.Int {
  case off, on, auto
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public enum CameraOutputMode {
  case stillImage, videoWithMic, videoOnly
  public static func == (a: OkayID.CameraOutputMode, b: OkayID.CameraOutputMode) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum CaptureResult {
  case success(content: OkayID.CaptureContent)
  case failure(Swift.Error)
}
public enum CaptureContent {
  case imageData(Foundation.Data)
  case image(UIKit.UIImage)
  case asset(Photos.PHAsset)
}
extension OkayID.CaptureContent {
  public var asImage: UIKit.UIImage? {
    get
  }
  public var asData: Foundation.Data? {
    get
  }
}
public enum CaptureError : Swift.Error {
  case noImageData
  case invalidImageData
  case noVideoConnection
  case noSampleBuffer
  case assetNotSaved
  public static func == (a: OkayID.CaptureError, b: OkayID.CaptureError) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension OkayID.CameraState : Swift.Equatable {}
extension OkayID.CameraState : Swift.Hashable {}
extension OkayID.CameraDevice : Swift.Equatable {}
extension OkayID.CameraDevice : Swift.Hashable {}
extension OkayID.CameraFlashMode : Swift.Equatable {}
extension OkayID.CameraFlashMode : Swift.Hashable {}
extension OkayID.CameraFlashMode : Swift.RawRepresentable {}
extension OkayID.CameraOutputMode : Swift.Equatable {}
extension OkayID.CameraOutputMode : Swift.Hashable {}
extension OkayID.CaptureError : Swift.Equatable {}
extension OkayID.CaptureError : Swift.Hashable {}

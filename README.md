# OkayID

Requires iOS 15.5+ and Swift 5

## Contents

- [Installation](#installation)
- [Usage](#usage)
 - [MYKad Capture](#mykad-capture)
    - [Configuration](#configuration)
  - [Base64 Conversion](#base64-conversion)
- [Error Handling for MYKad Capture/Passport Capture](#error-handling-for-mykadpassport-capture)
- [Example Project](#example)
- [Result](#result)

## Installation
OkayID Mobile is available through [CocoaPods](https://cocoapods.org). To install
it, add the following to your app target in your podfile:

```ruby
  use_frameworks!
  pod 'OkayID'

  post_install do |installer|
    installer.pods_project.targets.each do |target|
      if target.name == "CryptoSwift"
        puts "Enable module stability for CryptoSwift"
        target.build_configurations.each do |config|
            config.build_settings['BUILD_LIBRARY_FOR_DISTRIBUTION'] = 'YES'
        end
      end
    end
```

You will need to add `NSCameraUsageDescription` to your `info.plist` for camera permissions.

## Usage

Start by importing `OkayID` module into your swift file.

```swift
import OkayID
```

### MYKad / Passport Capture

1- Instantiate the configuration object. You must pass a reference to your navigation controller.

```swift
 let config = OkayIDConfig(viewController: nav)
```

The configuration object is initialized with default values.

2- Start the process by calling `start` on `OkayID` class. You can handle the result in the completion handler.

```swift
OkayIDDoc.start(
    okayIDConfig: config,
    license: yourLicense,
    { filePaths, jsonReturnString, error in
        // handle
    }
)
```

You will receieve `filePaths`, `json return string values` or an `error`. Two will be valid and the other will be `nil`.

#### Configuration

```swift
let config = OkayIDConfig(viewController: nav)
config.captureConfigPair = CaptureConfigPair(
            frontPhoto: OkayIDCaptureConfig(outputPath: myCustomFilePath))
config.id = IDType.MYS_MYKAD
```

Properties on `OkayIDConfig`:

| Property          | Default                                                                                                              | Type                 |
| :---------------- | :------------------------------------------------------------------------------------------------------------------- | :------------------- |
| captureConfigPair          | `CaptureConfigPair(frontPhoto: OkayIDCaptureConfig(outputPath: nil))`  | `CaptureConfigPair` |
| id          | `IdType.MYS_MYKAD` <br />`IdType.MYS_MYPR` <br />`IdType.MYS_MYKAS` <br />`IdType.MYS_MYTENTERA` <br /><br />`IdType.PASSPORT_MRZ` <br />|`IdType`  |  

#### ID Types

 -| Property name | Description |Default value
|--|--|--|--|
||`idType`|`IdType.MYS_MYKAD` <br />`IdType.MYS_MYPR` <br />`IdType.MYS_MYKAS` <br />`IdType.MYS_MYTENTERA` <br /><br />`IdType.PASSPORT_MRZ` <br />|`IdType.MYS_MYKAD`|

### Base64 Conversion

As long as you have imported `OkayID` module, then you have access to the following function:

```swift
do {
    let base64String = try convertImageToBase64(fileUrl: myImageFilePath)
    print(base64String)
} catch  {
    print(error.localizedDescription)
}
```
## Error Handling for MYKad/Passport Capture

The error received in the completion handler for both document/face capture is of the type `OkayCamError`:

```swift
public enum OkayIDError: Error {
    // Error due to wrong ID taken
     case unrecognisedIDType(_ msg: String)
   // Error due to invalid parameter value in config object.
    case invalidConfig(_ msg: String)
    // Error in capturing image.
    case imageCapture(_ msg: String)
    // Error due to user clicking cancel and going back.
    case userCancelled
    // Error due to invalid license usage.
    case invalidLicense
    // No error at all.
    case noError
     // Error due to camera permission denied by user.
    case cameraPermission
     // Error due to camera device.
    case camera(_ msg: String)
    // Any other errors.
    case miscellaneous(_ msg: String)
}
```
## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.


## Result

-  result callback has 3 params as follow

|Params | Possible value|
|--|--|
|success| will be true if the the image is captured successfully ,otherwise false 
|response| if its successful, returns filepath image, returns array of strings, otherwise will be null
|exception| if its successful, exception will be null otherwise, it contains the exception that occurred during the image capturing

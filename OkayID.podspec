Pod::Spec.new do |s|
    s.name             = 'OkayID'
    s.version          = '1.0.1'
    s.license          = { :file => 'LICENSE', :type => 'Commercial' }
    s.author           = { 'Innov8tif' => 'ekyc.team@innov8tif.com' }
    s.homepage         = 'https://innov8tif.com'
    s.summary          = 'OkayID Mobile SDK.'
    s.source           = { :git => 'https://gitlab.com/innov8tif-public/cocoapods/okayid-mobile-sdk', :tag => 'v1.0.1', :branch => 'master' }
    
    s.ios.deployment_target = '15.5'
    s.vendored_frameworks = 'OkayID.xcframework'

    s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

    
    s.swift_version = '5.0'
    s.dependency 'CryptoSwift', '~> 1.5.1'
    s.dependency 'LibTorch', '~>1.10.0'

   


 end
  

# Release Notes

## v1.0.0
### Features:

#### MYKad & Passport ID Scanning and Capture 
- Malaysian ID Card
    - ID scanning and OCR reading for Malaysian ID Card
    - Return ID card values from as JSON format (Refer to README)
- Passport 
    - Passport scanning and OCR reading for passport
    - Return passport values from mrz keys as JSON format (Refer to README)

#### Cropping features
- Malaysian ID Card
    - Uses opencv feature matching to crop ID card perfectly
    - ID card templates are use to match ID card

- Passport
    - Passport segmented using Paddle lite to crop image

#### OCR features
- Malaysian ID Card
    - Uses Paddle lite OCR to recognise the ID text
    - Use coordinates to pin point location of ID text
- Passport
    - Uses Paddle Lite OCR to read the MRZ key from passport and extracts the text values
    

